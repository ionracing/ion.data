﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public class FileNetworkClient : DataClient
    {
        string path;
        public FileNetworkClient(string path)
        {
            this.path = path;
        }

        public override byte[] Receive()
        {
            return File.ReadAllBytes(path);
        }

        public override void Send(byte[] data)
        {
            throw new NotImplementedException();
        }
    }
}
