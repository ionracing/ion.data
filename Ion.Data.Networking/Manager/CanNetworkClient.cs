﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public class CanNetworkClient : DataClient, IParseClient
    {
        NetworkClient client;
        public CanNetworkClient(SerialPort port)
        {
            NetworkManager.BindClient(port, this);
        }

        public CanNetworkClient(Stream stream)
        {
            NetworkManager.BindClient(stream, this);
        }

        void IParseClient.Initialize(NetworkClient client)
        {
            this.client = client;
            client.AddParser(new byte[] { 0xFF, 0xFF }, ParseCanMessage);
        }

        public NetworkPack ParseCanMessage(NetworkReader reader)
        {
            byte[] data = new byte[3];
            reader.Read(data);
            /*ushort tempData = (ushort)(data[1] << 8);
            tempData |= data[2];
            data = BitConverter.GetBytes(tempData);*/
            //byte[] endByte = new byte[1];
            //reader.Read(endByte);
            return new NetworkPack(new CanHeader(), data);
        }

        public override byte[] Receive()
        {
            return client.ReadMessage<CanHeader>(0).Data;
        }

        public override void Send(byte[] data)
        {
            Console.WriteLine("Sendt!");
            client.Reader.Write(new CanHeader().GetBytes(data));
        }

    }

    public class CanHeader : NetworkHeader
    {
        public CanHeader()
        {
            Id = 0;
        }

        public byte[] GetBytes(byte[] data)
        {
            List<byte> allBytes = new List<byte>();
            allBytes.Add(0xFF);
            allBytes.Add(0xFF);
            allBytes.AddRange(data);
            //allBytes.Add(0x2A);
            return allBytes.ToArray();
        }
    }
}
