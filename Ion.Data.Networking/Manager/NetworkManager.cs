﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public class NetworkManager
    {
        static Dictionary<Stream, NetworkClient> streamClients = new Dictionary<Stream, NetworkClient>();
        static Dictionary<SerialPort, NetworkClient> portClients = new Dictionary<SerialPort, NetworkClient>();

        public NetworkManager()
        {

        }

        public static NetworkClient BindClient(Stream stream)
        {
            if (!streamClients.ContainsKey(stream))
                streamClients.Add(stream, new NetworkClient(stream));
            return streamClients[stream];
        }

        public static NetworkClient BindClient(SerialPort port)
        {
            if (!portClients.ContainsKey(port))
                portClients.Add(port, new NetworkClient(port));
            return portClients[port];
        }

        public static void BindClient(Stream Stream, IParseClient client)
        {
            NetworkClient netClient = BindClient(Stream);
            client.Initialize(netClient);
        }

        public static void BindClient(SerialPort port, IParseClient client)
        {
            NetworkClient netClient = BindClient(port);
            client.Initialize(netClient);
        }
    }

    public delegate NetworkPack Parser(NetworkReader reader);

    public class LengthComparer : IComparer<byte[]>
    {
        public bool Reverse { get; set; }
        public LengthComparer(bool reverse)
        {
            Reverse = reverse;
        }

        int IComparer<byte[]>.Compare(byte[] x, byte[] y)
        {
            int number = x.Length - y.Length;
            return Reverse ? number * -1 : number;
        }
    }




    public class NetworkPack
    {
        public NetworkPack(NetworkHeader header, byte[] data)
        {
            this.Header = header;
            this.Data = data;
        }
        public virtual NetworkHeader Header { get; private set; }
        public virtual byte[] Data { get; protected set; }
    }

    public class NetworkHeader
    {
        public int Id { get; protected set; }
    }




    public struct DataWrapper
    {
        public ushort SensorID { get; set; }
        public int Value { get; set; }
        public int TimeStamp { get; set; }

        public byte[] GetBytes(bool shortFormat)
        {

            byte[] bytes;
            if (!shortFormat)
            {
                bytes = new byte[6];
                Array.Copy(BitConverter.GetBytes(SensorID), 0, bytes, 0, 2);
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 2, 4);
            }
            else
            {
                bytes = new byte[3];
                bytes[0] = (byte)SensorID;
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 1, 2);
            }
            return bytes;
        }

        public byte[] GetBytesWithTime()
        {
            byte[] bytes = new byte[10];
            bytes[0] = (byte)SensorID;
            bytes[1] = (byte)(SensorID >> 8);
            bytes[2] = (byte)Value;
            bytes[3] = (byte)(Value >> 8);
            bytes[4] = (byte)(Value >> 16);
            bytes[5] = (byte)(Value >> 24);
            bytes[6] = (byte)TimeStamp;
            bytes[7] = (byte)(TimeStamp >> 8);
            bytes[8] = (byte)(TimeStamp >> 16);
            bytes[9] = (byte)(TimeStamp >> 24);
            return bytes;

        }

        public static DataWrapper ReadData(byte[] bytes, int offset, bool shortFormat)
        {
            DataWrapper wrapper = new DataWrapper();
            if (!shortFormat)
            {
                wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
                wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
            }
            else
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = BitConverter.ToInt16(bytes, offset + 1);
            }
            return wrapper;
        }

        public static DataWrapper ReadDataWithTime(byte[] bytes, int offset)
        {
            DataWrapper wrapper = new DataWrapper();
            wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
            wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
            wrapper.TimeStamp = BitConverter.ToInt32(bytes, offset + 6);

            return wrapper;
        }
    }

    public abstract class DataClient
    {
        public abstract byte[] Receive();
        public abstract void Send(byte[] data);
        public bool ShortMode { get; set; }
        public byte[] LastRecivedBytes { get; private set; }

        public DataWrapper[] ReadDataWrappers(bool WithTime)
        {
            LastRecivedBytes = Receive();
            List<DataWrapper> AllWrappers = new List<DataWrapper>();
            for (int i = 0; i < LastRecivedBytes.Length; )
            {
                if (WithTime)
                {
                    AllWrappers.Add(DataWrapper.ReadDataWithTime(LastRecivedBytes, i));
                    i += 10;
                }
                else
                {
                    AllWrappers.Add(DataWrapper.ReadData(LastRecivedBytes, i, ShortMode));
                    i += 3;
                }
                
            }
            return AllWrappers.ToArray();
        }

        public void SendDataWrappers(DataWrapper[] wrappers)
        {
            List<byte> bytes = new List<byte>();
            foreach (DataWrapper wrap in wrappers)
            {
                bytes.AddRange(wrap.GetBytes(true));
            }
            Send(bytes.ToArray());
        }
    }

    public static class Extension
    {
        public static int GetSum(this byte[] bytes)
        {
            int sum = 0;
            for (int i = 0; i < bytes.Length / 2; i += 2)
            {
                sum += BitConverter.ToUInt16(bytes, i);
            }
            return sum;
        }
    }
}
