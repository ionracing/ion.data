﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public class NetworkClient
    {
        Dictionary<byte[], Parser> lookup = new Dictionary<byte[], Parser>();
        public SerialPort Port { get; private set; }
        private Stream stream { get; set; }
        public Stream Stream { get { return Port == null ? stream : Port.BaseStream; } }
        List<NetworkPack> allPacks = new List<NetworkPack>();
        public int LongestKey { get; private set; }
        public NetworkReader Reader { get; private set; }

        private NetworkClient()
        {
            lookup = new Dictionary<byte[], Parser>(/*new LengthComparer(true)*/);
            //TODO: Implement a form of SortedDictionary
            LongestKey = 0;
            Reader = new NetworkReader(this);
        }

        public NetworkClient(Stream stream)
            : this()
        {
            this.stream = stream;
        }

        public NetworkClient(SerialPort port)
            : this()
        {
            this.Port = port;
        }

        public void AddParser(byte[] sequence, Parser parser)
        {
            lookup.Add(sequence, parser);
            if (sequence.Length > LongestKey)
                LongestKey = sequence.Length;
        }

        public NetworkPack FindPack<T>(int id)
        {
            foreach (NetworkPack pack in allPacks)
            {
                if (pack.Header.GetType() == typeof(T) && pack.Header.Id == id)
                    return pack;
            }
            return null;
        }

        public NetworkPack ReadMessage<T>(int id)
        {
            NetworkPack pack = FindPack<T>(id);
            if (pack != null)
                allPacks.Remove(pack);
            while (pack == null)
            {
                byte[] key = Reader.AddjustToMessage();
                NetworkPack np = lookup[key](Reader);
                if (np != null)
                {
                    if (np.Header.GetType() == typeof(T) && np.Header.Id == id)
                        pack = np;
                    else
                        allPacks.Add(np);
                }
            }
            return pack;
        }

        public byte[] FindHeaderStart(Func<int, byte[]> peek)
        {
            if (lookup.Keys.Count == 0)
                throw new IndexOutOfRangeException();
            byte[] bytes = peek(LongestKey);
            //Console.WriteLine("Bytes: {0} {1}", bytes[0], bytes[1]);
            foreach (KeyValuePair<byte[], Parser> pair in lookup)
            {
                bool match = true;
                for (int i = 0; i < pair.Key.Length; i++)
                {
                    if (pair.Key[i] != bytes[i])
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    return pair.Key;
                }
            }
            return new byte[0];
        }
    }

    public class NetworkReader
    {
        NetworkClient client;
        byte[,] buffer = new byte[2, 1024];
        int currentIndex { get; set; }
        int totalIndex { get; set; }
        int available { get { return totalIndex - currentIndex; } }

        public NetworkReader(NetworkClient client)
        {
            this.client = client;
            currentIndex = 0;
            totalIndex = 0;
        }

        public void Read(byte[] buffer)
        {
            Read(buffer, 0, buffer.Length);
        }

        public void Read(byte[] buffer, int offset)
        {
            Read(buffer, offset, buffer.Length - offset);
        }

        public void Read(byte[] buffer, int offset, int length)
        {
            byte[] bytes = PeekBytes(length);
            Array.Copy(bytes, 0, buffer, offset, length);
            currentIndex += bytes.Length;
        }

        public void Write(byte[] buffer)
        {
            Write(buffer, 0, buffer.Length);
        }

        public void Write(byte[] buffer, int offset, int length)
        {
            client.Stream.Write(buffer, offset, length);
        }

        private BufferIndex ConvertToBufferIndex(int i)
        {
            return new BufferIndex(i % 1024, i / 1024 % 2);
        }

        private void FillBuffer()
        {
            byte[] tempBuffer = new byte[1024];
            int read = client.Stream.Read(tempBuffer, 0, tempBuffer.Length);
            //string s = "";
            for (int i = 0; i < read; i++)
            {
                //s += tempBuffer[i] +  " ";
                BufferIndex indexes = ConvertToBufferIndex(i + totalIndex);
                buffer[indexes.BufIndex, indexes.RealIndex] = tempBuffer[i];
            }
            //Console.WriteLine(s);
            totalIndex += read;
        }

        private void FillBuffer(int size)
        {
            while (available < size)
                FillBuffer();
        }

        private byte[] PeekBytes(int length)
        {
            FillBuffer(length);

            byte[] tempBuffer = new byte[length];
            for (int i = 0; i < length; i++)
            {
                BufferIndex bufferIndex = ConvertToBufferIndex(i + currentIndex);
                tempBuffer[i] = buffer[bufferIndex.BufIndex, bufferIndex.RealIndex];
            }
            return tempBuffer;
        }

        private class BufferIndex
        {
            public int RealIndex { get; private set; }
            public int BufIndex { get; private set; }

            public BufferIndex(int RealIndex, int BufIndex)
            {
                this.RealIndex = RealIndex;
                this.BufIndex = BufIndex;
            }
        }

        public byte[] AddjustToMessage()
        {
            FillBuffer(client.LongestKey);
            byte[] key;
            while ((key = client.FindHeaderStart(PeekBytes)).Length == 0)
            {
                currentIndex += 1;
                FillBuffer(client.LongestKey);
            }
            currentIndex += key.Length;
            return key;
        }
    }

    public interface IParseClient
    {
        void Initialize(NetworkClient client);
    }

}
