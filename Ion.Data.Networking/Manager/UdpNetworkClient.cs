﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Manager
{
    public class UdpNetworkClient : DataClient, IParseClient
    {
        NetworkClient baseClient;
        public ushort UdpPort { get; private set; }
        public ushort DestPort { get; private set; }

        private UdpNetworkClient(ushort port)
        {
            this.UdpPort = port;
        }

        public UdpNetworkClient(SerialPort port, ushort udpPort)
            : this(udpPort)
        {
            NetworkManager.BindClient(port, this);
        }

        public UdpNetworkClient(Stream stream, ushort udpPort)
            : this(udpPort)
        {
            NetworkManager.BindClient(stream, this);
        }

        public void Connect(ushort destinationPort)
        {
            DestPort = destinationPort;
        }

        public override void Send(byte[] bytes)
        {
            Send(bytes, ShortMode);
        }

        public void Send(byte[] data, bool shortFormat)
        {
            UdpHeader header;
            if (shortFormat)
                header = new UdpHeader(0, DestPort, (ushort)data.Length);
            else
                header = new UdpHeader(UdpPort, DestPort, (ushort)data.Length);
            byte[] bytes = header.GetBytes(data, shortFormat);
            baseClient.Reader.Write(bytes);
        }

        public override byte[] Receive()
        {
            return baseClient.ReadMessage<UdpHeader>(UdpPort).Data;
        }

        void IParseClient.Initialize(NetworkClient client)
        {
            baseClient = client;
            client.AddParser(new byte[] { 0x69, 0x4f }, ParseShortUdpHeader);
            client.AddParser(new byte[] { 0x49, 0x4f }, ParseUdpHeader);
        }

        NetworkPack ParseUdpHeader(NetworkReader reader)
        {
            byte[] buffer = new byte[8];
            byte[] data = new byte[0];

            reader.Read(buffer);
            ushort sourcePort = BitConverter.ToUInt16(buffer, 0);
            ushort destinationPort = BitConverter.ToUInt16(buffer, 2);
            ushort length = BitConverter.ToUInt16(buffer, 4);
            ushort checksum = BitConverter.ToUInt16(buffer, 6);
            if (length > 255)
                return null;
            data = new byte[length];
            reader.Read(data);
            UdpHeader header = new UdpHeader(sourcePort, destinationPort, length, checksum);
            if (header.Checksum != header.CalculateCheckSum(data))
                return null;
            return new NetworkPack(header, data);

        }

        NetworkPack ParseShortUdpHeader(NetworkReader reader)
        {
            byte[] buffer = new byte[4];
            byte[] data = new byte[0];

            reader.Read(buffer);
            //Console.WriteLine("Bytes: {0} {1} {2} {3}", buffer[0], buffer[1], buffer[2], buffer[3]);
            byte destinationPort = buffer[0];
            byte length = buffer[1];
            ushort checksum = BitConverter.ToUInt16(buffer, 2);
            if (length > 24)
                return null;
            data = new byte[length];
            reader.Read(data);
            UdpHeader header = new UdpHeader(0, destinationPort, length, checksum);
            if (header.Checksum != header.CalculateCheckSum(data))
                return null;
            return new NetworkPack(header, data);
        }
    }


    public class UdpHeader : NetworkHeader
    {
        public ushort SourcePort { get; set; }
        public ushort DestinationPort { get; set; }
        public ushort Length { get; set; }
        public ushort Checksum { get; set; }

        public UdpHeader(ushort sourcePort, ushort destinationPort, ushort length)
        {
            this.SourcePort = sourcePort;
            this.DestinationPort = destinationPort;
            this.Length = length;
            this.Id = DestinationPort;
        }

        public UdpHeader(ushort sourcePort, ushort destinationPort, ushort length, ushort checksum)
        {
            this.SourcePort = sourcePort;
            this.DestinationPort = destinationPort;
            this.Length = length;
            this.Checksum = checksum;
            this.Id = DestinationPort;
        }

        public byte[] GetBytes(byte[] data, bool shortFormat)
        {
            Length = (ushort)data.Length;
            Checksum = CalculateCheckSum(data);
            List<byte> bytes = new List<byte>();
            if (!shortFormat)
            {
                //TODO: implement magic bytes better
                bytes.AddRange(new byte[] { 0x49, 0x4f });
                bytes.AddRange(BitConverter.GetBytes(SourcePort));
                bytes.AddRange(BitConverter.GetBytes(DestinationPort));
                bytes.AddRange(BitConverter.GetBytes(Length));
            }
            else
            {
                bytes.AddRange(new byte[] { 0x69, 0x4f });
                bytes.Add((byte)DestinationPort);
                bytes.Add((byte)Length);
            }
            bytes.AddRange(BitConverter.GetBytes(Checksum));
            bytes.AddRange(data);
            return bytes.ToArray();
        }

        public void AddCheckSum(byte[] data)
        {
            Checksum = CalculateCheckSum(data);
        }

        public ushort CalculateCheckSum(byte[] data)
        {
            int checkSumCalc = 0;
            checkSumCalc = SourcePort + DestinationPort + Length + data.GetSum();
            while (checkSumCalc >= ushort.MaxValue)
            {
                checkSumCalc = (checkSumCalc >> 8) + (checkSumCalc & byte.MaxValue);
            }
            return (ushort)checkSumCalc;
        }
    }
}
