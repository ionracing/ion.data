﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Data.Networking.Logging
{
    public class LogManagerServer : LogManager
    {
        UdpClient logListner = new UdpClient(9965);
        TcpListener logClient = new TcpListener(IPAddress.Loopback, 9965);
        ILogger logServer;

        public LogManagerServer()
        {
            logServer = base.CreateLogger("LOGSERVER");
            logClient.Start();
            Listen();
            UdpListen();
        }

        public async void UdpListen()
        {
            UdpReceiveResult result = await logListner.ReceiveAsync();
            UdpListen();

            RemoteLogEntry entry = RemoteLogEntry.FromByteArray(result.Buffer);
            base.WriteCustom(entry);
        }

        public async void Listen()
        {
            TcpClient result = await logClient.AcceptTcpClientAsync();
            Listen();
            LogEntry[] entries = base.ListEntries();

            Stream s = result.GetStream();
            BinaryWriter bw = new BinaryWriter(s);
            bw.Write(entries.Length);
            foreach (LogEntry le in entries)
            {
                RemoteLogEntry.Convert(le).ToWriter(bw);
            }
            bw.Flush();
            bw.Close();

            //base.WriteCustom(entry);
        }
    }
}
