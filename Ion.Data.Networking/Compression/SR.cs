﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.IO.Compression
{
    class SR
    {
        public static string ArgumentNeedNonNegative { get; internal set; } = "The argument must be non-negative.";
        public static string CannotBeEmpty { get; internal set; } = "String cannot be empty.";
        public static string CDCorrupt { get; internal set; } = "Central Directory corrupt.";
        public static string CentralDirectoryInvalid { get; internal set; } = "Central Directory is invalid.";
        public static string CreateInReadMode { get; internal set; } = "Cannot create entries on an archive opened in read mode.";
        public static string CreateModeCapabilities { get; internal set; } = "Cannot use create mode on a non-writeable stream.";
        public static string CreateModeCreateEntryWhileOpen { get; internal set; } = "Entries cannot be created while previously created entries are still open.";
        public static string CreateModeWriteOnceAndOneEntryAtATime { get; internal set; } = "Entries in create mode may only be written to once, and only one entry may be held open at a time.";
        public static string DateTimeOutOfRange { get; internal set; } = "The DateTimeOffset specified cannot be converted into a Zip file timestamp.";
        public static string DeletedEntry { get; internal set; } = "Cannot modify deleted entry.";
        public static string DeleteOnlyInUpdate { get; internal set; } = "Delete can only be used when the archive is in Update mode.";
        public static string DeleteOpenEntry { get; internal set; } = "Cannot delete an entry currently open for writing.";
        public static string EntriesInCreateMode { get; internal set; } = "Cannot access entries in Create mode.";
        public static string EntryNameEncodingNotSupported { get; internal set; } = "The specified entry name encoding is not supported.";
        public static string EntryNamesTooLong { get; internal set; } = "Entry names cannot require more than 2^16 bits.";
        public static string EntryTooLarge { get; internal set; } = "Entries larger than 4GB are not supported in Update mode.";
        public static string EOCDNotFound { get; internal set; } = "End of Central Directory record could not be found.";
        public static string FieldTooBigCompressedSize { get; internal set; } = "Compressed Size cannot be held in an Int64.";
        public static string FieldTooBigLocalHeaderOffset { get; internal set; } = "Local Header Offset cannot be held in an Int64.";
        public static string FieldTooBigNumEntries { get; internal set; } = "Number of Entries cannot be held in an Int64.";
        public static string FieldTooBigOffsetToCD { get; internal set; } = "Offset to Central Directory cannot be held in an Int64.";
        public static string FieldTooBigOffsetToZip64EOCD { get; internal set; } = "Offset to Zip64 End Of Central Directory record cannot be held in an Int64.";
        public static string FieldTooBigStartDiskNumber { get; internal set; } = "Start Disk Number cannot be held in an Int64.";
        public static string FieldTooBigUncompressedSize { get; internal set; } = "Uncompressed Size cannot be held in an Int64.";
        public static string FrozenAfterWrite { get; internal set; } = "Cannot modify entry in Create mode after entry has been opened for writing.";
        public static string HiddenStreamName { get; internal set; } = "A stream from ZipArchiveEntry has been disposed.";
        public static string IO_DirectoryNameWithData { get; internal set; } = "Zip entry name ends in directory separator character but contains data.";
        public static string IO_ExtractingResultsInOutside { get; internal set; } = "Extracting Zip entry would have resulted in a file outside the specified destination directory.";
        public static string LengthAfterWrite { get; internal set; } = "Length properties are unavailable once an entry has been opened for writing.";
        public static string LocalFileHeaderCorrupt { get; internal set; } = "A local file header is corrupt.";
        public static string NumEntriesWrong { get; internal set; } = "Number of entries expected in End Of Central Directory does not correspond to number of entries in Central Directory.";
        public static string OffsetLengthInvalid { get; internal set; } = "The offset and length parameters are not valid for the array that was given.";
        public static string ReadingNotSupported { get; internal set; } = "This stream from ZipArchiveEntry does not support reading.";
        public static string ReadModeCapabilities { get; internal set; } = "Cannot use read mode on a non-readable stream.";
        public static string ReadOnlyArchive { get; internal set; } = "Cannot modify read-only archive.";
        public static string SeekingNotSupported { get; internal set; } = "This stream from ZipArchiveEntry does not support seeking.";
        public static string SetLengthRequiresSeekingAndWriting { get; internal set; } = "SetLength requires a stream that supports seeking and writing.";
        public static string SplitSpanned { get; internal set; } = "Split or spanned archives are not supported.";
        public static string UnexpectedEndOfStream { get; internal set; } = "Zip file corrupt: unexpected end of stream reached.";
        public static string UnsupportedCompression { get; internal set; } = "The archive entry was compressed using an unsupported compression method.";
        public static string UpdateModeCapabilities { get; internal set; } = "Update mode requires a stream with read, write, and seek capabilities.";
        public static string UpdateModeOneStream { get; internal set; } = "Entries cannot be opened multiple times in Update mode.";
        public static string WritingNotSupported { get; internal set; } = "This stream from ZipArchiveEntry does not support writing.";
        public static string Zip64EOCDNotWhereExpected { get; internal set; } = "Zip 64 End of Central Directory Record not where indicated.";

        internal static string Format(string exceptionString, EndOfStreamException exception)
        {
            return string.Format("{0}: {1}", exceptionString, exception.ToString());
        }
    }
}
